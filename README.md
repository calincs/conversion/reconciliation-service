# Structured Data Reconciliation Service

This project contains the components that make up the reconciliation API for structured data. This service works as a module within [NSSI](https://gitlab.com/calincs/conversion/NSSI) and is used by [VERSD](https://gitlab.com/calincs/conversion/versd). This service can be used to efficiently match bibliographic records in a structured data file (eg. csv, tsv, json, parquet) with [VIAF](http://viaf.org/), [Getty ULAN](https://www.getty.edu/research/tools/vocabularies/ulan/), and [Wikidata](https://www.wikidata.org/) bibliographic entities.

- [API server](api_server/)

- [S3 scripts and file structure](s3_scripts/)

- [Spark image](https://gitlab.com/calincs/conversion/reconciliation-spark-image) (in a separate project)

- [Authority files](https://gitlab.com/calincs/conversion/reconciliation-authority-files) (in a separate project)

For more information, see [this service’s wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home).
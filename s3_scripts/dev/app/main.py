import sys
from pyspark import SparkFiles
from pyspark.sql.functions import udf
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import types


def get_spark(job_id: str):
	spark = SparkSession.builder\
		.appName(job_id)\
		.getOrCreate()

	udfs = [
		("jaro_winkler_sim", "JaroWinklerSimilarity", types.DoubleType()),
		("jaccard_sim", "JaccardSimilarity", types.DoubleType()),
		("cosine_distance", "CosineDistance", types.DoubleType()),
		("Dmetaphone", "DoubleMetaphone", types.StringType()),
		("QgramTokeniser", "QgramTokeniser", types.StringType()),
		("Q3gramTokeniser", "Q3gramTokeniser", types.StringType()),
		("Q4gramTokeniser", "Q4gramTokeniser", types.StringType()),
		("Q5gramTokeniser", "Q5gramTokeniser", types.StringType()),
	]

	for a, b, c in udfs:
		spark.udf.registerJavaFunction(a, "uk.gov.moj.dash.linkage." + b, c)

	spark.sparkContext.setLogLevel("WARN")
	spark.conf.set("spark.sql.shuffle.partitions", "1000")
	spark.conf.set("spark.default.parallelism", "1000")

	# Set checkpoint directly for spark
	spark.sparkContext.setCheckpointDir("/opt/spark/work-dir/python/app/")
	return spark


if __name__ == "__main__":

	job_id = str(sys.argv[1])
	s3_data_path = sys.argv[6]

	# connect to spark
	spark = get_spark(job_id)

	# get python dependencies that are hosted in the spark image
	dependencies = ["link.py", "blocking.py", "normalize_data.py", "settings.py", "utils.py"]
	for file in dependencies:
		python_dep_file_path = SparkFiles.get(file)
		spark.sparkContext.addPyFile(python_dep_file_path)

	from link import run_linking
	results = run_linking(spark, sys.argv[1:])

	output_path = s3_data_path + "output/" + job_id.split("_")[0] + "-temp"

	# Don't output anything if there were no results
	try:
		results.coalesce(1).write.format('json').save(output_path)
	except Exception as error:
		print(error)

	spark.stop()

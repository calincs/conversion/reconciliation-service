# S3 Scripts

`reconciliation-service/s3_scripts/`(https://gitlab.com/calincs/conversion/reconciliation-service/-/tree/master/s3_scripts) shows the directory structure that should be present in LINCS S3 at `S3://users/recon-api/`. It also contains the scripts that run in the Spark Drivers for each job.

## Linking Scripts

`dev`, `stage`, `prod` each contain a directory `app`, which contain their version of the main.py script. These scripts initiate the linking of input records with an authority file of choice. Once the job has been requested by the [api server](api_server), the main.py script within the corresponding deployment folder creates a Spark session and calls on the scripts in the [Spark image](https://gitlab.com/calincs/conversion/reconciliation-spark-image) to handle the linking process. You can find additional details in the [wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home).

Typically, the three copies of the main.py script should be the same, unless new changes are being tested in the `dev` version. **When changes to `dev/app/main.py` are finalized, those changes must be manually copied to `stage/app/main.py` and `prod/app/main.py`.** GitLab CI/CD is set up in `.gitlab-ci.yml` so that each of those three files are automatically updated in S3 when changes are pushed to GitLab.

## User Data File Structure

`dev`, `stage`, `prod` each contain a directory `data`, showing the structure of where`input` and `output` data from jobs will be stored in S3. NSSI will create a folder for each job-id in those respective folders. Input data is deleted automatically by the reconciliation service upon job completion. Output data remains until scheduled clean-up is done by LINCS.

## Authority Extraction File Structure

`authority_extraction` shows the file structure that should be present at `S3://users/recon-api/authority_extraction/` for the [authority extraction scripts](https://gitlab.com/calincs/conversion/reconciliation-authority-files) to save the authority files against which user data is linked. `dev` is used for local and development deployments, while `prod` is used for stage and production deployments of the reconciliation service and reconciliation spark image. 

Each authority folder has a `release` and an `archive` folder. Typically, the data in `release` will be used by the reconciliation service. When an update to an authority begins, the data from `release` will be copied to `archive` and the reconciliation service will switch to use `archive` until the update is complete.

FROM python:3.9-slim-buster
USER root
# RUN printf '#!/bin/sh\nexit 0' > /usr/sbin/policy-rc.d
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-transport-https \
    curl \
    nano \
    libpq-dev gcc \
    python3-dev \
    python3-psycopg2 
    # software-properties-common \
    # python3.8 \
    # python3-pip \
    # python-is-python3

# RUN export PATH=/usr/lib/postgresql/X.Y/bin/:$PATH

RUN mkdir -p /usr/src/spark-on-k8s-operator/sparkctl
WORKDIR /usr/src/spark-on-k8s-operator/sparkctl
COPY api_server/sparkctl ./sparkctl
RUN chmod +x sparkctl \
    && cp sparkctl /usr/local/bin/sparkctl

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && mkdir -p /root/.kube

RUN python3 -m pip --no-cache-dir install --upgrade \
    minio \    
    pip \
    pika \
    setuptools\
    requests\
    psycopg2

WORKDIR /usr/src/app
COPY api_server/app .

# create config from CI variable and run the Rabbit Message Queue listener
CMD printenv KUBE_CONFIG >/root/.kube/config \
    && python -u ./consumer.py
# Might be better to use k8s lifecycle to create the config file in the container spec
#   lifecycle:
#     postStart:
#       exec:
#         command: ["/bin/sh", "-c", {{cmd}}]

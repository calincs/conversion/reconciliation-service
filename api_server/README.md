# API Server

This service allows you to submit bibliographic records to be reconciled against an authority file. This service works as a module within [NSSI](https://gitlab.com/calincs/conversion/NSSI). You can find details on using the service in the [wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home).
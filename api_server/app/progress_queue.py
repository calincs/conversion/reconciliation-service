import pika
import time
import os
from rabbitmq_helpers import get_pika
from s3_helpers import get_s3_paths, get_s3_credentials, minio_init
from spark_helpers import status_spark, kill_spark
from results import process_results
from delete import delete_input
from result_queue import results_message


# Sends a job status message to the RabbitMQ progress queue
def update_progress(job_id, status):
	from spark_helpers import status_spark
	queue = 'progressQueue'
	properties = pika.spec.BasicProperties(
		priority=0,
		delivery_mode=2,
		content_encoding="UTF-8",
		content_type="application/json")
	job_id = job_id[4:]

	if status == "ready":
		body = f'{{"jobId":{job_id}, "numCompleted":1, "completed":"true"}}'
	elif status == "in_progress":
		body = f'{{"jobId":{job_id}, "numStarted":1, "numCompleted":0, "completed":"false"}}'
	elif status == "cancelled":
		body = f'{{"jobId":{job_id}, "cancelled":"true"}}'
	elif status == "failed":
		body = f'{{"jobId":{job_id}, "failed":"true"}}'

	connection = get_pika()
	channel = connection.channel()

	try:
		arguments = {'x-dead-letter-exchange': 'appMessages.dlx'}
		channel.queue_declare(
			queue=queue,
			passive=False,
			durable=True,
			exclusive=False,
			auto_delete=False,
			arguments=arguments)

		# bind the progress queue to the app messages exchange in rmq
		channel.queue_bind(exchange='appMessagesExchange', queue=queue, routing_key=queue)

	except ValueError as err:
		print(f"[job {job_id}] RMQ progress queue connection error: ", err)

	channel.basic_publish(
		exchange='',
		routing_key=queue,
		body=body,
		properties=properties)

	print(f"[job-{job_id}] Message sent to progressQueue: ", body)

	connection.close()


## TODO delete job input data if spark could not be started
def poll_status(job_id, target_kg, job_start_time):
	# polls to see if a job is done
	# updates RMQ progressqueue when the job is complete or if the job failed
	# updates RMQ resultsqueue when the job is complete
	s3_access, s3_secret = get_s3_credentials()
	_, _, sparkctl_path, _, s3_output_file_path, bucket = get_s3_paths()
	client = minio_init(s3_access, s3_secret)

	done = False
	while not done:

		# check if the spark job succeeded and if there are output folders in the jobID-temp bucket
		job_status = status_spark(client, s3_output_file_path, job_id, sparkctl_path, bucket, temp=True)
		if job_status == 1:

			done = True

			final_results_status = process_results(client, s3_output_file_path, job_id, target_kg, sparkctl_path, bucket, job_start_time)

			if final_results_status == 1:
				print(f'[{job_id}] Job is complete.')
				results_message(job_id)
				update_progress(job_id, "ready")

				# Comment this line to keep spark driver alive after job succeeds
				kill_spark(job_id, sparkctl_path)

				# Comment this line to keep the input data in S3 after a job succeeds
				delete_input(job_id)
			else:
				print(f'[{job_id}] Job failed.')
				update_progress(job_id, "failed")
				delete_input(job_id)

				# Comment this line to keep keep spark driver alive after job fails
				#kill_spark(job_id, sparkctl_path)

		elif job_status == 0:
			# Job is still in progress
			done = False

		elif job_status == 2:

			done = True
			print(f'[{job_id}] Job failed.')
			update_progress(job_id, "failed")

			# Comment this line to keep the input data in S3 after a job fails
			delete_input(job_id)

			# Un-comment kill_spark() if you want the spark driver to be deleted if it fails
			# Useful to comment while troubleshooting
			# Set to keep failed jobs in review environments but delete in stage/prod

			# deploy_target = os.environ.get("DEPLOY_TARGET")
			# if "review" not in deploy_target:
			# 	kill_spark(job_id, sparkctl_path)

		# TODO check if the job got stuck and stopped due to error
		elif job_status is None:
			done = True
			print(f'[{job_id}] Job failed or does not exist.')
			update_progress(job_id, "failed")

		time.sleep(20)

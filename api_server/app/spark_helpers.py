import subprocess
import os
from s3_helpers import minio_check_success, get_s3_paths, get_s3_db_paths


# creates a yaml file with the job id as the filename and the sparkl job name
# yaml_path should be a directory containing a reconcile-template.yaml file
def create_yaml(yaml_dir, yaml_template, job_id, target_kg, input_format, match_number, match_threshold):
	with open(yaml_template, 'r') as template:
		data = template.readlines()
		_, _, _, s3_path, _, _ = get_s3_paths()
		_, _, s3_authority_path, _, bucket = get_s3_db_paths(target_kg, "release")
		s3_app_path = s3_path[:-11]  # recon-api/dev/ or recon-api/stage/ or recon-api/prod/
		s3_data_path = s3_path[:-6]  # recon-api/dev/data/ or recon-api/stage/data/ or recon-api/prod/data/
		deploy_target = os.environ.get("DEPLOY_TARGET")

		for index in range(0, len(data)):
			if "metadata" in data[index]:
				if "job-name" in data[index + 1]:
					# set "metadata.name: job_id"
					data[index + 1] = f"  name: {job_id}\n"
			elif "mainApplicationFile" in data[index]:
				data[index] = f"  mainApplicationFile: s3a://{bucket}/{s3_app_path}app/main.py\n"
			elif "job_id" in data[index]:
				# set an input argument to be the job id for the python code
				data[index] = f'    - "{job_id}_{deploy_target.strip()}"\n'
			elif "target_kg" in data[index]:
				# specify which authority file to reconcile the data against
				data[index] = f'    - "{target_kg}"\n'
			elif "input_format" in data[index]:
				# specify the format of the input data
				data[index] = f'    - "{input_format}"\n'
			elif "match_number" in data[index]:
				# specify the maximum number of potential matches to return for each entity
				data[index] = f'    - "{match_number}"\n'
			elif "match_threshold" in data[index]:
				# specify the minimum probability threshold for a potential match to be returned
				data[index] = f'    - "{match_threshold}"\n'
			elif "S3_data_path" in data[index]:
				data[index] = f'    - "S3a://{bucket}/{s3_data_path}"\n'
			elif "S3_authority_path" in data[index]:
				data[index] = f'    - "S3a://{bucket}/{s3_authority_path}"\n'
			elif "image: " in data[index]:
				# SPARK IMAGE is set based on the deployment variable in .gitlab-ci.yml file
				data[index] = f'  image: {os.environ.get("SPARK_IMAGE")}\n'

	yaml_path = f"{yaml_dir}/{job_id}.yaml"
	with open(yaml_path, 'w') as new_yaml:
		new_yaml.writelines(data)

	return yaml_path


# delete the file labelled job_ID.yaml in the yaml_dir directory
def delete_yaml(yaml_dir, job_id):
	os.remove(yaml_dir + "/" + job_id + ".yaml")


# example command: sparkctl -n spark-jobs create s3-test.yaml
def start_spark(yaml_dir, yaml_template, job_id, target_kg, input_format, sparkctl_path, match_number, match_threshold):
	# create a yaml file for this spark job
	yaml_path = create_yaml(yaml_dir, yaml_template, job_id, target_kg, input_format, match_number, match_threshold)

	cp = subprocess.run(
		[sparkctl_path, "-n", "spark-jobs", "create", yaml_path],
		universal_newlines=True,
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE)

	if cp.stderr != "":
		print(f"[{job_id}] Spark cluster could not be started")
		print(cp.stderr)
		return 0
	else:
		print(cp.stdout)
		return 1


# example command: sparkctl -n spark-jobs status <SparkApplication name>
# returns 1 if there is a success file in the output folder
# returns 0 if spark job is in progress
# returns 2 if spark job failed
# returns None if spark job does not exist and there is no success file in the output folder
# temp is a boolean that says whether the data should be checked for in a bucket ending in -temp
def status_spark(client, s3_output_file_path, job_id, sparkctl_path, bucket, temp):

	# check the status of the spark job
	cp = subprocess.run(
		[sparkctl_path, "-n", "spark-jobs", "status", job_id],
		universal_newlines=True,
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE)

	full_status = cp.stdout

	# check if the job is already done
	# if there is a success file in the job's output s3 folder, return 1
	if temp:
		job_id_s3_path = job_id + "-temp"
	else:
		job_id_s3_path = job_id
	job_status = minio_check_success(client, s3_output_file_path, job_id_s3_path, bucket)
	if job_status:
		return 1

	# if the spark job failed return 2
	elif "driver container failed" in full_status:
		return 2

	# if there is no success file in the output and the spark job does not exist then return None
	elif "failed to get SparkApplicaton" in full_status:
		return None

	# otherwise the job is still running. Return 0
	else:
		return 0


# example command: sparkctl -n spark-jobs delete s3-test
def kill_spark(job_id, sparkctl_path):
	cp = subprocess.run([sparkctl_path, "-n", "spark-jobs", "delete", job_id])

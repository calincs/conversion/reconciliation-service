import io
import json
import os
import requests
import re
from minio import Minio


def get_s3_credentials():
	# User must set environment variables for s3 credentials
	access = os.environ.get("LINCS_S3_ACCESS")
	secret = os.environ.get("LINCS_S3_SECRET")

	return access, secret


def get_env_bucket():
	# Get the name used within S3 paths based on the deployment (review, stage, prod)

	deploy_target = os.environ.get("DEPLOY_TARGET")

	if "review" in deploy_target:
		env_bucket = "dev"
	elif "stag" in deploy_target:
		env_bucket = "stage"
	else:
		env_bucket = "prod"

	return env_bucket


def get_s3_paths():
	# Get the S3 paths for input and output user files
	env_bucket = get_env_bucket()

	s3_input_file_path = f"recon-api/{env_bucket}/data/input/"
	s3_output_file_path = f"recon-api/{env_bucket}/data/output/"

	yaml_dir = "/usr/src/app/yaml"
	yaml_template = "/usr/src/app/reconcile-template.yaml"
	sparkctl_path = "/usr/src/spark-on-k8s-operator/sparkctl/sparkctl"

	bucket = "users"

	return yaml_dir, yaml_template, sparkctl_path, s3_input_file_path, s3_output_file_path, bucket


def get_s3_db_paths(authority, release_archive):
	# TODO Update this to check whether to use archive or release when an update is in progress.

	# release_archive should be a string either "release" or "archive"

	# Get the S3 paths for authority files to load into the database

	env_bucket = get_env_bucket()
	if env_bucket == "dev":
		env_bucket = "dev"
	else:
		# production or stage
		env_bucket = "prod"

	base_path = f"recon-api/authority_extraction/{env_bucket}/{authority.replace('-', '_').replace('_sample', '')}/{release_archive}"

	s3_db_context_path = f"{base_path}/{authority.replace('_', '-')}_db_context/"
	s3_db_id_path = f"{base_path}/{authority.replace('_', '-')}_db_id/"
	s3_db_authority_path = f"{base_path}/{authority.replace('_', '-')}_cleaned.parquet"
	metadata_path = f"{base_path}/metadata.json"

	bucket = "users"

	return s3_db_context_path, s3_db_id_path, s3_db_authority_path, metadata_path, bucket


# Connect to Minio S3 storage
# This is where the files to compare and the comparison scripts are stored
def minio_init(access, secret):
	return Minio(
		'aux.lincsproject.ca',
		access_key=access,
		secret_key=secret,
		secure=True,
		region='ca')


def get_minio_client():
	access, secret = get_s3_credentials()
	client = minio_init(access, secret)
	return client


# Upload a local file (input_bytes) to the s3 bucket (client) and save it at output_path
# Takes input data in bytes and converts it into a stream
# If downloaded is True then assume json string input
# If downloaded is False then assume byte object as data
def minio_put_file(client, output_path, input_bytes, file_type, bucket_name, job_id, downloaded):
	if file_type == "json" and not downloaded:
		json_string = json.dumps(input_bytes, ensure_ascii=False)
		input_bytes = json_string.encode()

	input_stream = io.BytesIO(input_bytes)
	try:
		client.put_object(bucket_name, output_path, input_stream, len(input_bytes))
		print(f"[{job_id}] Data successfully uploaded to S3.")
		return 1
	except Exception as err:
		print(f"[{job_id}] File could not be uploaded to s3 storage. \n")
		print(err)
		return 0


def minio_list_objects(client, bucket, file_prefix):
	object_list = []
	objects = client.list_objects(bucket, recursive=True, prefix=file_prefix)

	for object in objects:
		object_list.append(object)
	return object_list


def minio_get_object(client, bucket_name, object_name):
	try:
		response = client.get_object(bucket_name, object_name)
		data = ""
		for d in response.stream(32 * 1024):
			data_decode = d.decode()
			data_decode_clean = data_decode.replace("'", '"')
			if data == "":
				data = data_decode_clean
			else:
				data = data + data_decode_clean
		response.close()
		response.release_conn()
	except Exception as err:
		print("S3 get object error: ", err)
	return data


def minio_fget_object(client, bucket_name, object_name, filename):
	client.fget_object(bucket_name, object_name, filename)


# Delete a file from the s3 bucket (client) stored at file_path
# https://stackoverflow.com/questions/57664545/how-to-remove-a-path-in-minio-storage-using-python-sdk#:~:text=to%20delete%20an%20entire%20folder,that%20you%20intend%20to%20delete.
def minio_delete_file(client, file_path, bucket):
	try:
		objects = client.list_objects(bucket, file_path, recursive=True)
		for item in objects:
			client.remove_object(bucket, item.object_name)
		return 1
	except:
		print("Error deleting S3 file")
		return 0


# Checks that the file type is a supported data format
# Returns a string representing a supported format or False if not supported
def get_filetype(file_name):
	if file_name.endswith(".csv"):
		return "csv"
	elif file_name.endswith(".tsv"):
		return "tsv"
	elif file_name.endswith(".parquet"):
		return "parquet"
	elif file_name.endswith(".json"):
		return "json"
	else:
		return False


# Checks the success of a job
# Return 1 if there is a success file in the job's output folder
# Return 0 if there is no success file
def minio_check_success(client, dir_path, job_id, bucket):
	obj_path = dir_path + job_id + "/_SUCCESS"
	try:
		client.stat_object(bucket, obj_path)
		return 1
	except:
		return 0


def get_nssi_file(job_id, document_uri):

	# For testing locally to access machine's local host from within api_server docker container
	# local_host = os.environ.get("RABBIT_HOST")
	# document_uri = document_uri.replace("localhost", local_host)

	#request_header = {"Authorization": auth_token}
	try:
		#response = requests.get(document_uri, headers=request_header)
		response = requests.get(document_uri)

	except ConnectionRefusedError as err:
		print(f"[{job_id}] Job cancelled. Cannot retrieve data from NSSI:", err, "\n")
		return None, None

	status = response.status_code
	if status == 200:
		response_headers = response.headers
		filename = re.search('filename="(.*)"', response_headers["Content-Disposition"])
		if filename:
			input_format = get_filetype(filename.group(1))
			data = response.content
			return data, input_format
		else:
			print(f"[{job_id}] Retrieved data from NSSI but could not process file type.")
			return None, None
	else:
		print(f"[{job_id}] Cannot retrieve data from NSSI (status: {status}).\n Request sent to: {document_uri}\n Response received: {response}")
		return None, None

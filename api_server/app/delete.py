from s3_helpers import get_minio_client, minio_delete_file, get_s3_paths
from spark_helpers import kill_spark


# delete the input file from s3
def delete_input(job_id):
	# connect to s3 bucket
	client = get_minio_client()
	yaml_dir, yaml_template, sparkctl_path, s3_input_file_path, s3_output_file_path, bucket = get_s3_paths()

	# delete the input file for the job
	minio_delete_file(client, s3_input_file_path + job_id + "/", bucket)
	print(f'[{job_id}] Input data deleted from S3.')


# delete the output file for job with name job_id
def delete_output(job_id):
	# connect to s3 bucket
	client = get_minio_client()
	yaml_dir, yaml_template, sparkctl_path, s3_input_file_path, s3_output_file_path, bucket = get_s3_paths()

	# delete the output file for the job
	minio_delete_file(client, s3_output_file_path + job_id + "/", bucket)
	print(f'[{job_id.split("-temp")[0]}] Data deleted from S3.')


# TODO: ./sparkctl -n spark-jobs delete job-5
# if a job is in progress, it cancels it and deletes all associated and spark driver
# if a job is finished, deletes all associated data and spark driver
def cancel_job(job_id):
	from progress_queue import update_progress

	# connect to s3 bucket
	client = get_minio_client()
	yaml_dir, yaml_template, sparkctl_path, s3_input_file_path, s3_output_file_path, bucket = get_s3_paths()

	try:
		# delete the input file for the job
		minio_delete_file(client, s3_input_file_path + job_id + "/", bucket)

		# delete the output file for the job
		minio_delete_file(client, s3_output_file_path + job_id + "/", bucket)

		# delete the spark driver for the job
		kill_spark(job_id)

		# update rabbitmq status
		update_progress(job_id, "cancelled")

	except Exception as err:
		# TODO send error to rabbitmq
		print(f"[{job_id}] could not cancel job: ", err)

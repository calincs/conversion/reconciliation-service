from delete import cancel_job
from rabbitmq_helpers import ack_message
import json
import functools


def cancel_request(ch, method, properties, body, args):
	(conn, thrds) = args

	print(body)
	print(properties)
	try:
		body = json.loads(body)
		job_id = "job-" + str(body["jobId"])

		if job_id is not None:
			print(f"[{job_id}] Delete request received in expected format.")
			cancel_job(job_id)

			# acknowledge the job has processed
			cb = functools.partial(ack_message, ch, method.delivery_tag)
			conn.add_callback_threadsafe(cb)

		else:
			print(f"[{job_id}] Job could not be deleted.", "\n")
	except Exception as err:
		print(f"[{job_id}] Job could not be deleted. ", err, "\n")

		# acknowledge the job has processed
		cb = functools.partial(ack_message, ch, method.delivery_tag)
		conn.add_callback_threadsafe(cb)

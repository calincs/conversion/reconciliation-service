from psycopg2 import connect, DatabaseError
from s3_helpers import get_s3_credentials, get_minio_client, get_s3_db_paths, minio_list_objects, minio_fget_object
import os
import csv


def db_connect():

	print("Connecting to ", os.getenv("DATABASE_URL"))
	conn = connect(os.getenv("DATABASE_URL"))
	cur = conn.cursor()
	return conn, cur


def create_database():
	reload_wikidata = os.environ.get("RELOAD_WIKIDATA")
	reload_getty = os.environ.get("RELOAD_GETTY")
	reload_viaf = os.environ.get("RELOAD_VIAF")

	try:
		# connect to postgres
		conn, cur = db_connect()

		# connect to S3
		access, secret = get_s3_credentials()
		s3_client = get_minio_client()

		# Load data for each authority
		for authority in [
					"viaf_works",
					"viaf_works_sample",
					"viaf_expressions",
					"viaf_expressions_sample",
					"wikidata_works",
					"wikidata_works_sample",
					"wikidata_authors",
					"wikidata_authors_sample",
					"getty_people_sample",
					"getty_people"]:
			print("Authority: ", authority)

			s3_db_context_path, s3_db_id_path, _, _, bucket = get_s3_db_paths(authority, "release")

			file_list = minio_list_objects(s3_client, bucket, s3_db_context_path)
			for file in file_list:
				if file.object_name.endswith(".tsv"):
					print("Reading file: ", file.object_name)
					minio_fget_object(s3_client, bucket_name=bucket, object_name=file.object_name, filename="temp/" + authority + "_context.tsv")
					with open("temp/" + authority + "_context.tsv") as f:
						context_file_contents = f.read()

			file_list = minio_list_objects(s3_client, bucket, s3_db_id_path)
			for file in file_list:
				if file.object_name.endswith(".tsv"):
					print("Reading file: ", file.object_name)
					minio_fget_object(s3_client, bucket_name=bucket, object_name=file.object_name, filename="temp/" + authority + "_id.tsv")
					with open("temp/" + authority + "_id.tsv") as f:
						id_file_contents = f.read()

			sample = ""
			if "sample" in authority:
				sample = "_sample"

			if "viaf" in authority:
				if reload_viaf.lower() == "true":
					if "works" in authority:
						init_viaf_works(cur, context_file_contents, id_file_contents, sample=sample)
					if "expressions" in authority:
						init_viaf_expressions(cur, context_file_contents, id_file_contents, sample=sample)

			elif "wikidata" in authority:
				if reload_wikidata.lower() == "true":
					if "works" in authority:
						init_wikidata_works(cur, context_file_contents, id_file_contents, sample=sample)
					if "authors" in authority:
						init_wikidata_authors(cur, context_file_contents, id_file_contents, sample=sample)

			elif "getty" in authority:
				if reload_getty.lower() == "true":
					if "people" in authority:
						init_getty_people(cur, context_file_contents, id_file_contents, sample=sample)

			print("*" * 20)

	except (Exception, DatabaseError) as error:
		print("Database error: ", error)
	finally:
		if conn is not None:
			conn.commit()
			conn.close()
			print('Database connection closed.')


def drop_table(cur, table_name):
	print(f"Dropping table: {table_name}")
	cur.execute(f"DROP TABLE IF EXISTS {table_name} CASCADE")


def create_table(cur, table_name, create_string):
	drop_table(cur, table_name)
	print(f"Table dropped: {table_name}")

	cur.execute(create_string)
	print(f"Table created: {table_name}")


def load_data(cur, table_name, csv_file_string):
	print("Loading data into table: ", table_name)
	lines = csv_file_string.splitlines()
	reader = csv.reader(lines, delimiter="\t")
	num_cols = len(next(reader))  # Skip the header row while getting number of columns
	col_string = str('%s, ' * num_cols)[0:-2]
	for row in reader:
		if row[0] != "":
			cur.execute(
				f"INSERT INTO {table_name} VALUES ({col_string})",
				row
			)


def init_viaf_works(cur, context_file, id_file, sample):
	# sample will be "_sample" or an empty string
	id_table = f"viaf_works{sample}_id"
	context_table = f"viaf_works{sample}_context"

	create_table(cur, id_table, f'CREATE TABLE {id_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "work_VIAF_URI" TEXT, "work_Worldcat_URI" TEXT, "work_Wikidata_URI" TEXT)')
	create_table(cur, context_table, f'CREATE TABLE {context_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "work_Name_1" TEXT, "work_Name_2" TEXT, "author_Name_1" TEXT, "author_Name_2" TEXT, "author_Language" TEXT, "author_CountryOrigin" TEXT, "work_Language" TEXT, "author_BirthYear" TEXT, "author_DeathYear" TEXT, "author_VIAF_URI" TEXT, "author_ISNI_ID" TEXT, "author_Worldcat_URI" TEXT, "author_Sex" TEXT, "author_Wikidata_URI" TEXT, FOREIGN KEY ("unique_id") REFERENCES {context_table} ("unique_id"))')

	for table in [[id_table, id_file], [context_table, context_file]]:
		load_data(cur, table[0], table[1])


def init_viaf_expressions(cur, context_file, id_file, sample):
	# sample will be "_sample" or an empty string
	id_table = f"viaf_expressions{sample}_id"
	context_table = f"viaf_expressions{sample}_context"

	create_table(cur, id_table, f'CREATE TABLE {id_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "expression_VIAF_URI" TEXT, "expression_Wikidata_URI" TEXT)')
	create_table(cur, context_table, f'CREATE TABLE {context_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "work_VIAF_URI" TEXT, "work_Name_1" TEXT, "work_Name_2" TEXT, "expression_Name_1" TEXT, "expression_Name_2" TEXT, "author_BirthYear" TEXT, "author_CountryOrigin" TEXT, "author_DeathYear" TEXT, "author_Language" TEXT, "author_Name_1" TEXT, "author_Name_2" TEXT, "author_Sex" TEXT, "expression_Language" TEXT, "author_VIAF_URI" TEXT, "author_ISNI_ID" TEXT, "author_Worldcat_URI" TEXT, "author_Wikidata_URI" TEXT, FOREIGN KEY ("unique_id") REFERENCES {context_table} ("unique_id"))')

	for table in [[id_table, id_file], [context_table, context_file]]:
		load_data(cur, table[0], table[1])


def init_wikidata_works(cur, context_file, id_file, sample):
	# sample will be "_sample" or an empty string
	id_table = f"wikidata_works{sample}_id"
	context_table = f"wikidata_works{sample}_context"

	create_table(cur, id_table, f'CREATE TABLE {id_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "work_Wikidata_URI" TEXT, "work_ISBN10_ID" TEXT, "work_ISBN13_ID" TEXT, "work_OCLC_ID" TEXT, "work_OCLCControl_ID" TEXT, "work_VIAF_URI" TEXT, "author_ISNI_ID" TEXT, "author_VIAF_URI" TEXT, "author_Wikidata_URI" TEXT)')
	create_table(cur, context_table, f'CREATE TABLE {context_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "work_Wikidata_URI" TEXT, "work_Name_1" TEXT, "work_Name_2" TEXT, "work_CountryOrigin_URI" TEXT, "work_CountryOrigin" TEXT, "work_Edition" TEXT, "work_Format_URI" TEXT, "work_Format" TEXT, "work_ISBN10_ID" TEXT, "work_ISBN13_ID" TEXT, "work_Language_URI" TEXT, "work_Language" TEXT, "work_OCLC_ID" TEXT, "work_OCLCControl_ID" TEXT, "work_PublicationDate" TEXT, "work_Publisher_URI" TEXT, "work_Publisher" TEXT, "work_VIAF_URI" TEXT, "author_ISNI_ID" TEXT, "author_VIAF_URI" TEXT, "author_Wikidata_URI" TEXT, "author_BirthYear" TEXT, "author_DeathYear" TEXT, "author_Name_1" TEXT, "author_Name_2" TEXT, "author_Sex" TEXT, "work_WikidataType_URI" TEXT, "work_WikidataType" TEXT, "work_Genre_URI" TEXT, "work_Genre" TEXT, "work_Description" TEXT, "author_Description" TEXT, FOREIGN KEY ("unique_id") REFERENCES {context_table} ("unique_id"))')

	for table in [[id_table, id_file], [context_table, context_file]]:
		load_data(cur, table[0], table[1])


def init_wikidata_authors(cur, context_file, id_file, sample):
	# sample will be "_sample" or an empty string

	id_table = f"wikidata_authors{sample}_id"
	context_table = f"wikidata_authors{sample}_context"

	create_table(cur, id_table, f'CREATE TABLE {id_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "author_ISNI_ID" TEXT, "author_VIAF_URI" TEXT, "author_Wikidata_URI" TEXT)')
	create_table(cur, context_table, f'CREATE TABLE {context_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "author_ISNI_ID" TEXT, "author_VIAF_URI" TEXT, "author_Wikidata_URI" TEXT, "author_BirthYear" TEXT, "author_DeathYear" TEXT, "author_Name_1" TEXT, "author_Name_2" TEXT, "author_Sex" TEXT, "work_WikidataType_URI" TEXT, "work_WikidataType" TEXT, "work_Genre_URI" TEXT, "work_Genre" TEXT, "author_Description" TEXT, FOREIGN KEY ("unique_id") REFERENCES {context_table} ("unique_id"))')

	for table in [[id_table, id_file], [context_table, context_file]]:
		load_data(cur, table[0], table[1])


def init_getty_people(cur, context_file, id_file, sample):
	# sample will be "_sample" or an empty string

	id_table = f"getty_people{sample}_id"
	context_table = f"getty_people{sample}_context"

	create_table(cur, id_table, f'CREATE TABLE {id_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "author_Getty_URI" TEXT)')
	create_table(cur, context_table, f'CREATE TABLE {context_table} ("unique_id" INTEGER NOT NULL PRIMARY KEY, "author_Getty_URI" TEXT, "author_BirthYear" TEXT, "author_DeathYear" TEXT, "author_Name_1" TEXT, "author_Name_2" TEXT, "author_Sex" TEXT, "author_Description" TEXT, "author_BirthPlace" TEXT, "author_DeathPlace" TEXT, "author_Nationality" TEXT, "author_Language" TEXT, "author_Role" TEXT, FOREIGN KEY ("unique_id") REFERENCES {context_table} ("unique_id"))')

	for table in [[id_table, id_file], [context_table, context_file]]:
		load_data(cur, table[0], table[1])


def get_record_context(conn, cur, target_kg, record_unique_id):
	# query postgres to get context for the record
	# returns a dictionary representing the record and all context
	try:

		merged_col_names = []
		merged_context = []

		for t in ["_context", "_id"]:
			table = target_kg.replace("-", "_") + t

			# get headings
			cur.execute(f"SELECT * FROM {table} LIMIT 0")
			col_names = [desc[0] for desc in cur.description]

			cur.execute(f'SELECT * FROM {table} WHERE "unique_id" = {record_unique_id}')
			# print("Query: ", f'SELECT * FROM {table} WHERE "unique_id" = {record_unique_id}')
			context = cur.fetchone()
			if context:
				context = list(context)
			else:
				print('No matching record in authority db table {table} for query: SELECT * FROM {table} WHERE "unique_id" = {record_unique_id} ')
				return {}

			merged_col_names = merged_col_names + col_names
			merged_context = merged_context + context

		record_dict = dict(zip(merged_col_names, merged_context))

		if context == []:
			print('No matching record in authority db table {table} for query: SELECT * FROM {table} WHERE "unique_id" = {record_unique_id} ')
			return {}

		return record_dict

	except (Exception, DatabaseError) as error:
		print("Database error: ", error)
		return {}
	finally:
		if conn is not None:
			conn.commit()

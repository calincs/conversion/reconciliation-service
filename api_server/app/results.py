from datetime import datetime
from s3_helpers import minio_get_object, minio_list_objects, minio_put_file, get_s3_db_paths
from operator import itemgetter
import json
from delete import delete_output
from postgres import get_record_context, db_connect


def combine_fields(input_dict):
	"""
	Takes a dictionary as input
	Combines values for keys that follow the pattern "name_#":"value" into "name":[value1, value2, etc]
	Outputs the new dictionary with only the combined fields
	"""

	keys = list(input_dict.keys())
	keys.sort()

	for key in keys:
		key_split = key.split("_")
		if key_split[-1].isdigit():
			# digit = key_split[-1]
			new_key = "_".join(key_split[:-1])

			if new_key in input_dict:
				if input_dict[key]:
					new_list = input_dict[new_key] + [input_dict[key]]
				else:
					new_list = input_dict[new_key]
			else:
				if input_dict[key]:
					new_list = [input_dict[key]]
				else:
					new_list = []
			input_dict[new_key] = new_list
			input_dict.pop(key)

	return input_dict


def strings_to_list(input_dict):
	"""
	Takes a dictionary as input
	For each value in the dictionary, if it is not a list it converts it to a list
	If the string is actually a comma separated list then it will be returned as a proper list of strings
	unique_id will remain a string
	"""

	keys = list(input_dict.keys())
	for key in keys:
		if key != "unique_id":
			if key != "match_probability":
				if isinstance(input_dict[key], str):
					if input_dict[key] == "":
						input_dict[key] = []
					else:
						input_dict[key] = input_dict[key].split(" , ")

	return input_dict


# Gets the data from S3 that was output by the spark job in the {jobID}-temp bucket
# Add context fields to the data
# Outputs the enhanced JSON, organized by input record ID, to the {jobID} bucket
def process_results(client, s3_output_file_path, job_id, target_kg, sparkctl_path, bucket, job_start_time):
	# Read the JSON from S3
	# Convert JSON to Dict
	# Query postgres for additional context
	# Restructure dict to organize by input record ID

	try:
		conn, cur = db_connect()

		# get a list of all the json files in the output bucket
		file_list = minio_list_objects(client, bucket, s3_output_file_path + job_id + "-temp")

		new_data = {}
		for file in file_list:
			if "json" in file.object_name:
				temp_data = minio_get_object(client, bucket, file.object_name)
				temp_data_split = temp_data.split("\n")

				for line in temp_data_split:
					if line != "":
						record = json.loads(line)

						if "unique_id" in record:
							if "unique_id_authority" in record:
								if "match_probability" in record:
									unique_id_user = record["unique_id"]
									unique_id_authority = str(record["unique_id_authority"])
									match_probability = record["match_probability"]

									# query authority db context table for this target_kg to get rest of context for this record
									record = get_record_context(conn, cur, target_kg, unique_id_authority)

									# we no longer need the internal unique ID. replace with the user's unique id for this record
									# keep the original match probability from spark
									record["unique_id"] = str(unique_id_user)
									record["match_probability"] = match_probability

									# group any fields ending in "_<number>" into lists
									record = combine_fields(record)

									# convert each output field into a list
									record = strings_to_list(record)

									if unique_id_user in new_data:
										record_list = new_data[unique_id_user] + [record]
										new_data[unique_id_user] = record_list
									else:
										new_data[unique_id_user] = [record]
		conn.close()

		sorted_data = []
		for i in new_data:
			sorted_data.append(sorted(new_data[i], key=itemgetter('match_probability'), reverse=True))

		# save the final results to S3
		minio_put_file(client, f"{s3_output_file_path}{job_id}/output.json", sorted_data, "json", bucket, job_id, False)

		# Get, enhance, and return metadata about the job and authority file used
		_, _, _, metadata_path, _ = get_s3_db_paths(target_kg, "release")

		metadata = json.loads(minio_get_object(client, bucket, metadata_path))

		current_time = datetime.now()
		duration = current_time - job_start_time
		try:
			metadata["authorityID"] = str(target_kg)
			metadata["jobStart"] = str(job_start_time)
			metadata["jobEnd"] = str(current_time)
			metadata["jobDuration"] = str(duration.total_seconds()) + "s"
		except Exception as err:
			print(f"[{job_id}] Unable to retrieve or return metadata: {err}")

		# Create a job specific version of the metadata
		minio_put_file(client, f"{s3_output_file_path}{job_id}/metadata.json", metadata, "json", bucket, job_id, False)

		# Delete the temporary output bucket
		delete_output(job_id + "-temp")

		return 1
	except Exception as err:
		print(f"[{job_id}] Process results error: {err}")
		return 0

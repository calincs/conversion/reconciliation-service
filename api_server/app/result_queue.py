import pika
from rabbitmq_helpers import get_pika
from s3_helpers import get_s3_paths


# Sends a job status message to the RabbitMQ progress queue of the form
'''
{
	“jobId”: 1234,
	“resultStoreType”: “S3”,
	“resultStoreParams”: {
	“S3_BUCKET_NAME”: “users”,
	”S3_OBJECT”: “recon-api/prod/data/output/job-1234/output.json”,
	”S3_METADATA_OBJECT”: “recon-api/prod/data/output/job-1234/metadata.json”
	}
}
'''


def results_message(job_id):
	queue = 'resultQueue'
	properties = pika.spec.BasicProperties(
		priority=0,
		delivery_mode=2,
		content_encoding="UTF-8",
		content_type="application/json")
	job_id = job_id[4:]
	_, _, _, _, s3_output_path, _ = get_s3_paths()

	body = f'{{"jobId": {job_id},"resultStoreType": "S3","resultStoreParams": {{"S3_BUCKET_NAME": "users","S3_OBJECT_NAME": "{s3_output_path}job-{job_id}/output.json","S3_METADATA_OBJECT": "{s3_output_path}job-{job_id}/metadata.json"}}}}'

	connection = get_pika()
	channel = connection.channel()

	try:
		arguments = {'x-dead-letter-exchange': 'appMessages.dlx'}
		channel.queue_declare(
			queue=queue,
			passive=False,
			durable=True,
			exclusive=False,
			auto_delete=False,
			arguments=arguments)

		# bind the results queue to the app messages exchange in rmq
		channel.queue_bind(exchange='appMessagesExchange', queue=queue, routing_key=queue)

	except ValueError as err:
		print(f"[{job_id}] Results message error: ", err)

	channel.basic_publish(
		exchange='',
		routing_key=queue,
		body=body,
		properties=properties)

	print(f"[job-{job_id}] Message sent to resultsQueue: ", body)
	connection.close()

from rabbitmq_helpers import get_pika
from create_job import handle_request
from cancel_job import cancel_request
from postgres import create_database
from pika import exceptions
import os
import sys
import time
import functools


# RabbitMQ consumer
def main(queue):

	# Check that all database tables exist
	# Load data when needed
	create_database()

	# maximum number of spark jobs that can run at one time. Default to 1.
	num_jobs = os.environ.get("CONCURRENT_JOBS")
	if num_jobs is not None:
		try:
			int(num_jobs)
		except ValueError:
			num_jobs = 1
	else:
		num_jobs = 1

	while True:
		arguments = {'x-dead-letter-exchange': 'appMessages.dlx'}
		connection = get_pika()
		if connection is not False:
			channel = connection.channel()
			if channel:
				try:
					channel.queue_declare(
						queue=queue,
						passive=False,
						durable=True,
						exclusive=False,
						auto_delete=False,
						arguments=arguments)

					# bind the reconciliation queue to the app messages exchange in rmq
					channel.queue_bind(exchange='appMessagesExchange', queue=queue, routing_key=queue)

					# limit to one unackowledged message at a time
					channel.basic_qos(prefetch_count=num_jobs)

				except ValueError as err:
					print("[*] RMQ setup error: ", err)

				threads = []
				if queue == "reconciliationQueue":
					on_message_callback = functools.partial(handle_request, args=(connection, threads))
				elif queue == "reconciliationCancelQueue":
					on_message_callback = functools.partial(cancel_request, args=(connection, threads))
				try:
					channel.basic_consume(queue=queue, on_message_callback=on_message_callback, auto_ack=False)
				except ValueError as err:
					print("[*] RMQ consume error: ", err)

				print('[*] RabbitMQ consumer listening to ' + queue + '. Waiting for messages.')
				try:
					channel.start_consuming()
				except exceptions.ConnectionClosed:
					print(f"[{queue}] Lost Connection with RabbitMQ. Trying to reconnect every 60 seconds.")
					time.sleep(60)

				# Wait for all threads to complete
				for thread in threads:
					thread.join()

				connection.close()

			else:
				print(f"[{queue}] Trying to connect to RabbitMQ every 60 seconds.")
				time.sleep(60)
		else:
			print(f"[{queue}] Trying to connect to RabbitMQ every 60 seconds.")
			time.sleep(60)


if __name__ == '__main__':
	try:
		main("reconciliationQueue")
	except KeyboardInterrupt:
		print('Interrupted')
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)

from datetime import datetime
from s3_helpers import get_minio_client, minio_put_file, get_s3_paths, get_nssi_file
from spark_helpers import delete_yaml, start_spark
from rabbitmq_helpers import ack_message
from progress_queue import update_progress, poll_status
import json
import functools
import threading


def send_job(data, input_format, target_kg, match_number, match_threshold, job_id, document_uri, conn, ch, method, body):
	status = 1
	thread_id = threading.get_ident()
	print(f'[{job_id}] Thread id: %s Delivery tag: %s Message body: %s', thread_id, method.delivery_tag)

	# connect to s3 bucket
	client = get_minio_client()
	yaml_dir, yaml_template, sparkctl_path, s3_input_file_path, s3_output_file_path, bucket = get_s3_paths()

	if input_format == "download":
		data, input_format = get_nssi_file(job_id, document_uri)
		if data is None:
			status = 0
			print(f"[{job_id}] Job cancelled. No data submitted.")
			update_progress(job_id, "failed")
		else:
			print(f"[{job_id}] Data retrieved from NSSI.")
			status = minio_put_file(
				client,
				s3_input_file_path + job_id + "/data." + input_format,
				data,
				input_format,
				bucket,
				job_id,
				True)
			if status == 0:
				update_progress(job_id, "failed")
	else:
		status = minio_put_file(
			client,
			s3_input_file_path + job_id + "/data." + input_format,
			data,
			input_format,
			bucket,
			job_id,
			False)
		if status == 0:
			update_progress(job_id, "failed")

	if status == 1:
		# start spark script in s3
		# a yaml file for this job is created using the job id
		status = start_spark(
			yaml_dir,
			yaml_template,
			job_id,
			target_kg,
			input_format,
			sparkctl_path,
			match_number,
			match_threshold)
		if status == 0:
			update_progress(job_id, "failed")

	# delete the yaml file for this job
	delete_yaml(yaml_dir, job_id)

	if status == 1:
		# update status to say that the job started
		update_progress(job_id, "in_progress")

		# start process to poll job status and update RMQ progress and results queues
		poll_status(job_id, target_kg, datetime.now())

	# acknowledge the job has processed
	cb = functools.partial(ack_message, ch, method.delivery_tag)
	conn.add_callback_threadsafe(cb)


# parse the request body and send the data for processing
def handle_request(ch, method, properties, body, args):
	(conn, thrds) = args
	try:
		body = json.loads(body)
		job_id = "job-" + str(body["jobId"])
		target_kg = body["context"]["authority"]
		document_uri = None

		# get data from request body or uri for data location
		# priority is given to the request body over the uri
		if "documentURI" in body:
			if body["documentURI"] is not None:
				document_uri = body["documentURI"]
				input_format = "download"
				data = None
		if "document" in body:
			if body["document"] is not None:
				data = json.loads(body["document"])
				input_format = "json"
				document_uri = "n/a"

		# set defaults
		match_number = "3"
		match_threshold = "0.6"

		# set default values for optional parameters if not provided
		if "matchNumber" in body["context"]:
			match_number = str(body["context"]["matchNumber"])
		if "matchThreshold" in body["context"]:
			match_threshold = str(body["context"]["matchThreshold"])

		if document_uri is not None:
			print(f"[{job_id}] Request received in expected format.")
			t = threading.Thread(target=send_job, args=(data, input_format, target_kg, match_number, match_threshold, job_id, document_uri, conn, ch, method, body))
			t.start()
			thrds.append(t)
		else:
			print(f"[{job_id}] Job cancelled. No data submitted", "\n")
			update_progress(job_id, "failed")

	except KeyError as err:
		print(f"[{job_id}] Job cancelled. Request body missing an expected key:", err, "\n")
		update_progress(job_id, "failed")

		# acknowledge the job has processed
		cb = functools.partial(ack_message, ch, method.delivery_tag)
		conn.add_callback_threadsafe(cb)

	except TypeError as err:
		print(f"[{job_id}] Job cancelled. Invalid request:", err, "\n")
		update_progress(job_id, "failed")

		# acknowledge the job has processed
		cb = functools.partial(ack_message, ch, method.delivery_tag)
		conn.add_callback_threadsafe(cb)

	except ValueError as err:
		print(f"[{job_id}] Job cancelled. Invalid request:", err, "\n")
		update_progress(job_id, "failed")

		# acknowledge the job has processed
		cb = functools.partial(ack_message, ch, method.delivery_tag)
		conn.add_callback_threadsafe(cb)

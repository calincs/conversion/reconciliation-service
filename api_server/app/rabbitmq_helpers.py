import pika
import os


def get_rabbit_credentials():
	# Connect to a different RMQ instance depending on the deployment environment
	# These variables are set for the deployment environment in the .gitlab-ci.yml file
	rabbit_virtual_host = os.environ.get("RABBIT_VIRTUAL_HOST")
	rabbit_password = os.environ.get("RABBIT_PASSWORD")

	# These variables are consistent across deployment environments
	rabbit_username = os.environ.get("RABBIT_USERNAME")
	rabbit_host = os.environ.get("RABBIT_HOST")
	rabbit_port = os.environ.get("RABBIT_PORT")

	return rabbit_username, rabbit_password, rabbit_host, rabbit_port, rabbit_virtual_host


def get_pika():
	username, password, host, port, rabbit_virtual_host = get_rabbit_credentials()
	credentials = pika.PlainCredentials(username, password)
	try:
		connection = pika.BlockingConnection(
			pika.ConnectionParameters(
				host=host,
				port=port,
				virtual_host=rabbit_virtual_host,
				credentials=credentials,
				heartbeat=600,
				blocked_connection_timeout=300))
	except Exception as err:
		print("Failed to connect to RabbitMQ Queue. Confirm that RabbitMQ is running and check your credentials.")
		print("ERROR: ", err)
		return False
	return connection


def ack_message(ch, delivery_tag):
	"""Note that `ch` must be the same pika channel instance via which
	the message being ACKed was retrieved (AMQP protocol constraint).
	https://github.com/pika/pika/blob/1.0.1/examples/basic_consumer_threaded.py
	"""
	if ch.is_open:
		ch.basic_ack(delivery_tag)
	else:
		# Channel is already closed, so we can't ACK this message;
		# TODO: log and maybe delete job?
		pass
